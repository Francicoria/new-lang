#include <assert.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tokenizer.h"

static inline bool is_whitespace(char c) {
	return c == ' ' || c == '\n' || c == '\t';
}

void lexer_trim_left(Lexer *l) {
	while (l->content.len > 0 && is_whitespace(*l->content.src)) {
		l->content.src += 1;
		l->content.len -= 1;
	}
}

void lexer_trim_right(Lexer *l) {
	while (l->content.len > 0 && is_whitespace(l->content.src[l->content.len - 1])) {
		l->content.len -= 1;
	}
}

void lexer_trim(Lexer *l) {
	lexer_trim_left(l);
	lexer_trim_right(l);
}

bool StringV_equal(StringV s1, StringV s2) {
	if (s1.len != s2.len) return false;
	for (size_t i = 0; i < s1.len; ++i) if (s1.src[i] != s2.src[i]) return false;
	return true;
}
int parse_number(StringV s) {
	int result = 0;
	for (size_t i = 0; i < s.len; ++i) {
		if (!isdigit(s.src[i])) {
			assert(false && "implement error reporting of incorrect number literal"); // @todo
		}
		result = (result * 10) + (s.src[i] - '0');
	}
	return result;
}

const char *get_tt_name(enum TokenType tt) {
	switch (tt) {
#define X(n) case n: return #n;
TOKENTYPES
#undef X
	default: assert(false && "unreachable");
	}
}

Token next_token(Lexer *l) {
	lexer_trim(l);

	StringV current = {
		.src = l->content.src,
	};
	static_assert(TT_COUNT == 15);
	while (l->content.len > 0 && !is_whitespace(*l->content.src)) {
		switch (*l->content.src) {
		case '(':
		case ')':
		case '{':
		case '}':
		case '<':
		case '>':
		case ';':
		case ':': 
		case '=':
		case '+':
		case '-':
		case '*':
		case ',': {
			if (current.len == 0) {
				current.len += 1;
				l->content.src += 1;
				l->content.len -= 1;
			}
			goto skip;
		} break;
		default: break;
		}

		current.len += 1;
		l->content.src += 1;
		l->content.len -= 1;
	}
	skip:

	assert(current.len > 0);

	Token t = {0};
	static_assert(TT_COUNT == 15);

	if (isdigit(current.src[0])) {
		int x = parse_number(current);

		t.type = TT_NUMBER;
		t.value.number = x;
		return t;
	}

	if (current.len == 1) {
		switch (current.src[0]) {
		case '(': t.type = TT_OPENING_PAREN; return t;
		case ')': t.type = TT_CLOSING_PAREN; return t;
		case '{': t.type = TT_OPENING_BRACE; return t;
		case '}': t.type = TT_CLOSING_BRACE; return t;
		case '<': t.type = TT_OPENING_BRACKET; return t;
		case '>': t.type = TT_CLOSING_BRACKET; return t;
		case ';': t.type = TT_SEMICOLON; return t;
		case ':': t.type = TT_COLON;     return t;
		case '=': t.type = TT_EQUAL;     return t;
		case '+': t.type = TT_PLUS;      return t;
		case '-': t.type = TT_MINUS;     return t;
		case '*': t.type = TT_ASTERISK;  return t;
		case ',': t.type = TT_COMMA;     return t;
		
		default: break;
		}
	}

	// @todo: multi-char tokens
	current.src = memcpy(malloc(current.len), current.src, current.len); // duplicate the string
	t.type = TT_IDENTIFIER;
	t.value.identifier = current;

	return t;
}
void print_token(Token t) {
	printf("%s", get_tt_name(t.type));
	switch (t.type) {
	case TT_IDENTIFIER: printf("(\"" StringV_fmt "\")", StringV_arg(t.value.identifier)); break;
	case TT_NUMBER: printf("(%d)", t.value.number); break;
	default: break;
	}

	printf("\n");
}
