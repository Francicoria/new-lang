# new-lang

WIP language

## Compilation
```console
$ ./build.sh   # on linux
$ .\build.cmd  # on windows
```

## Usage
```console
$ ./new_lang
$ fasm out.asm
$ ./out
```
